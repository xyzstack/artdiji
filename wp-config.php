<?php
/**
 * WordPress için taban ayar dosyası.
 *
 * Bu dosya şu ayarları içerir: MySQL ayarları, tablo öneki,
 * gizli anahtaralr ve ABSPATH. Daha fazla bilgi için
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php düzenleme}
 * yardım sayfasına göz atabilirsiniz. MySQL ayarlarınızı servis sağlayıcınızdan edinebilirsiniz.
 *
 * Bu dosya kurulum sırasında wp-config.php dosyasının oluşturulabilmesi için
 * kullanılır. İsterseniz bu dosyayı kopyalayıp, ismini "wp-config.php" olarak değiştirip,
 * değerleri girerek de kullanabilirsiniz.
 *
 * @package WordPress
 */
if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "http://127.0.0.1"):

// ** MySQL ayarları - Bu bilgileri sunucunuzdan alabilirsiniz ** //
/** WordPress için kullanılacak veritabanının adı */
define( 'DB_NAME', 'artdiji' );

/** MySQL veritabanı kullanıcısı */
define( 'DB_USER', 'root' );

/** MySQL veritabanı parolası */
define( 'DB_PASSWORD', '' );

/** MySQL sunucusu */
define( 'DB_HOST', 'localhost' );

/** Yaratılacak tablolar için veritabanı karakter seti. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Veritabanı karşılaştırma tipi. Herhangi bir şüpheniz varsa bu değeri değiştirmeyin. */
define('DB_COLLATE', '');

elseif ($_SERVER['HTTP_HOST'] == "artdiji.com" || $_SERVER['HTTP_HOST'] == "www.artdiji.com"):

	// ** MySQL ayarları - Bu bilgileri sunucunuzdan alabilirsiniz ** //
/** WordPress için kullanılacak veritabanının adı */
define( 'DB_NAME', 'artdiji' );

/** MySQL veritabanı kullanıcısı */
define( 'DB_USER', 'root' );

/** MySQL veritabanı parolası */
define( 'DB_PASSWORD', 'dnsas23H6DSa1' );

/** MySQL sunucusu */
define( 'DB_HOST', 'localhost' );

/** Yaratılacak tablolar için veritabanı karakter seti. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Veritabanı karşılaştırma tipi. Herhangi bir şüpheniz varsa bu değeri değiştirmeyin. */
define('DB_COLLATE', '');

endif;

/**#@+
 * Eşsiz doğrulama anahtarları.
 *
 * Her anahtar farklı bir karakter kümesi olmalı!
 * {@link http://api.wordpress.org/secret-key/1.1/salt WordPress.org secret-key service} servisini kullanarak yaratabilirsiniz.
 * Çerezleri geçersiz kılmak için istediğiniz zaman bu değerleri değiştirebilirsiniz. Bu tüm kullanıcıların tekrar giriş yapmasını gerektirecektir.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'm1os!?SMVv-Y!9S|:&X5qLS:)42qgVh[SgF:fgd[Kc}y*C_p6@8)<4O)@(C l}*H' );
define( 'SECURE_AUTH_KEY',  ':bKEM9I7!koC]p_&@oW&C/O1^a*1=g]f8Aubm1b|1Ji_jC{oF%W3ywA(#c7!;0?I' );
define( 'LOGGED_IN_KEY',    '4jDV~DgtQZ@PSi+KXYV$.d*PY9?y[0l`dDC (H^n2Ckp#Hini6:&OjFOFb^v+uoO' );
define( 'NONCE_KEY',        '8u0H#;56S{tCN`1,T!`CFD^!4K+UYubUs=4 ogsp]EP9Y;8:I+pq*TP8C?OKj5AJ' );
define( 'AUTH_SALT',        'b/?f3L.^7r({/3J7N6(+=8Yu+j=,qAce!cZZWNuxbZhU/m/Dnpx?rJ1gpi:}KatK' );
define( 'SECURE_AUTH_SALT', '`_&uMfwlC&Ec>hZci=R6o_R>y7:&mfMKia,XxKXSdiv.{_UH?+WXagNA9$QbFh `' );
define( 'LOGGED_IN_SALT',   '*(HXYE9quUj)jV /1{{vV;IrC~-2ON9Q4^jdN=DVo~z> AaxollvMDXxUjZOCOhO' );
define( 'NONCE_SALT',       'asO5h=)Jg(x9goO6lk(ba#F*WVa~MqH#@0Bb_If!4}#]F]*>;t!g}3_,;NQ([ t^' );
/**#@-*/

/**
 * WordPress veritabanı tablo ön eki.
 *
 * Tüm kurulumlara ayrı bir önek vererek bir veritabanına birden fazla kurulum yapabilirsiniz.
 * Sadece rakamlar, harfler ve alt çizgi lütfen.
 */
$table_prefix = 'wp_';

/**
 * Geliştiriciler için: WordPress hata ayıklama modu.
 *
 * Bu değeri "true" yaparak geliştirme sırasında hataların ekrana basılmasını sağlayabilirsiniz.
 * Tema ve eklenti geliştiricilerinin geliştirme aşamasında WP_DEBUG
 * kullanmalarını önemle tavsiye ederiz.
 */
define('WP_DEBUG', false);

/* Hepsi bu kadar. Mutlu bloglamalar! */

/** WordPress dizini için mutlak yol. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** WordPress değişkenlerini ve yollarını kurar. */
require_once(ABSPATH . 'wp-settings.php');
